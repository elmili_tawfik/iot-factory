export  class Factory {
  userid: string;
  nbrSensor: number;
  name: string;
  description: string;
  lng: number;
  lat: number;
  sensorsId: [string];

}
