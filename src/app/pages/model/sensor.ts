export  class Sensor {

  code: string;
  userid: string;
  name:  string;
  value: string;
  factoryid: string;
}
