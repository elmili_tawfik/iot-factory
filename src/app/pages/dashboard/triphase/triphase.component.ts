import {AfterViewInit, Component, Input, OnDestroy} from '@angular/core';
import {NbThemeService} from '@nebular/theme';
import {HttpClient, HttpParams} from '@angular/common/http';

const Swal = require('sweetalert2');
import * as XLSX from 'xlsx';

@Component({
  selector: 'ngx-triphase',
  styleUrls: ['./triphase.component.scss'],
  templateUrl: './triphas.component.html',
})
export class TriphaseComponent {
  fileName = 'ExcelSheet.xlsx';
  last_updateex ;
  private data;
  private value = 0;
  private VoltagePhaseA;
  private VoltagePhaseB;
  private VoltagePhaseC;
  private CurrentPhaseA;
  private CurrentPhaseB;
  private CurrentPhaseC;

  private ActivePowerTotal;
  private ActivePowerPhaseA;
  private ActivePowerPhaseB;
  private ActivePowerPhaseC;
  private PowerFactorTotal;
  private PowerFactorPhaseA;
  private PowerFactorPhaseB;
  private PowerFactorPhaseC;
  //
  private ComActiveTotal;
  private PositiveActiveTotal;
  private ReverseActiveTotal;
  // gauge
  canvasWidth = 200;
  public bottomLabel = 'Kwh';
  public options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '120'],
    needleStartValue: 60,
  };
  public options2 = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '120'],
    needleStartValue: 20,
  };
  public options3 = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '240'],
    needleStartValue: 20,
  };
  public options4 = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '50'],
    needleStartValue: 30,
  };
  themeSubscription: any;
  name1: any = 'Com Active Total Energy';
  name2: any = 'Positive Active Total Energy';
  name3: any = 'Reverse Active Total Energy';
  name4: any = 'ActivePower';
  // needle
  needleValue_ComActiveTotal;
  needleValue_PositiveActiveTotal;
  needleValue_ReverseActiveTotal;
  needleValue_ActivePowerTotal;
  // label
  label_ComActiveTotal = '';
  label_PositiveActiveTotal = '';
  label_ReverseActiveTotal = '';
  label_ActivePowerTotal = '';
  last_update;

  option: any = {};

  constructor(private theme: NbThemeService, private http: HttpClient) {
    const optionss = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/sensors/sensor/findByType',
      {
        type: 'triphase',
      }, optionss).subscribe(data => {
      const resSTR = JSON.stringify(data);
      const resJSON = JSON.parse(resSTR);
      this.data = resJSON;
      this.data.forEach(item => {
        const couterCom = item.ConsomationTripahse.length - 1;
        const counterCV = item.Voltage_CurrentrTipahse.length - 1;
        const counterpos = item.PositiveTripahse.length - 1;
        const counterRev = item.ReverserTipahse.length - 1;
        const i = item.ActivePowerTipahse.length - 1;
        //
        this.VoltagePhaseA = item.Voltage_CurrentrTipahse[counterCV].VoltagePhaseA;
        this.VoltagePhaseB = item.Voltage_CurrentrTipahse[counterCV].VoltagePhaseB;
        this.VoltagePhaseC = item.Voltage_CurrentrTipahse[counterCV].VoltagePhaseC;
        this.CurrentPhaseA = item.Voltage_CurrentrTipahse[counterCV].CurrentPhaseA;
        this.CurrentPhaseB = item.Voltage_CurrentrTipahse[counterCV].CurrentPhaseB;
        this.CurrentPhaseC = item.Voltage_CurrentrTipahse[counterCV].CurrentPhaseC;
        //
        this.ActivePowerTotal = item.ActivePowerTipahse[i].ActivePowerTotal;
        this.ActivePowerPhaseA = item.ActivePowerTipahse[i].ActivePowerPhaseA;
        this.ActivePowerPhaseB = item.ActivePowerTipahse[i].ActivePowerPhaseB;
        this.ActivePowerPhaseC = item.ActivePowerTipahse[i].ActivePowerPhaseC;
        this.PowerFactorTotal = item.ActivePowerTipahse[i].PowerFactorTotal;
        this.PowerFactorPhaseA = item.ActivePowerTipahse[i].PowerFactorPhaseA;
        this.PowerFactorPhaseB = item.ActivePowerTipahse[i].PowerFactorPhaseB;
        this.PowerFactorPhaseC = item.ActivePowerTipahse[i].PowerFactorPhaseC;
        //
        this.needleValue_ComActiveTotal = item.ConsomationTripahse[couterCom].ComActiveTotal;
        this.label_ComActiveTotal = item.ConsomationTripahse[couterCom].ComActiveTotal.substring(2, 8);
        this.needleValue_ActivePowerTotal = item.ActivePowerTipahse[i].ActivePowerTotal;
        this.label_ActivePowerTotal = item.ActivePowerTipahse[i].ActivePowerTotal.substring(0, 6);

        this.needleValue_PositiveActiveTotal = item.PositiveTripahse[counterpos].PositiveActiveTotal;
        //
        this.label_PositiveActiveTotal = item.PositiveTripahse[counterpos].PositiveActiveTotal.substring(2, 8);

        this.label_ReverseActiveTotal = item.ReverserTipahse[counterRev].ReverseActiveTotal.substring(2, 8);
        this.needleValue_ReverseActiveTotal = item.ReverserTipahse[counterRev].ReverseActiveTotal;
        this.last_update = new Date(item.Voltage_CurrentrTipahse[counterCV].time);
        //
        const date = new Date(item.Voltage_CurrentrTipahse[counterCV].time);
        let min;
        date.getMinutes() > 10 ? (min = date.getMinutes()) : min = '0' + date.getMinutes();
        this.last_updateex = (date.getFullYear() + '/' + date.getDate() +
          '/' + (date.getMonth() + 1) + ' ' + date.getHours() + ':' + min + ' ');

      });
    });
  }

  onChange(event) {
    if (event === true) {
      Swal.fire('ON!',
        'Your Electric meter device is ON Now.',
        'success',
      );
    } else {
      Swal.fire('OFF!',
        'Your  Electric meter device is OFF Now.',
        'error',
      );
    }
  }

  exportexcel(): void {
    /* table id is passed over here */
    const element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);

  }
}
