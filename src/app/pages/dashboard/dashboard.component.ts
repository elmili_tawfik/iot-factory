import {Component, OnDestroy} from '@angular/core';
import {
  NbComponentStatus, NbGlobalPhysicalPosition, NbGlobalPosition,

  NbSearchService,
  NbThemeService, NbToastrService,
} from '@nebular/theme';
import {takeWhile} from 'rxjs/operators' ;
import {SolarData} from '../../@core/data/solar';
import {HttpClient, HttpParams} from '@angular/common/http';
import {DashService} from './dash.service';
import {ToasterConfig} from 'angular2-toaster';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

const Swal = require('sweetalert2');

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnDestroy {
  private alive = true;
  private test1;
  select: any;
  solarValue: number;
  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  /*
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };
*/


  statusCards: string;
  private data: any;
  private data2: any;
  private data3: any;
  selectedOption;
  public id_fac: any;
  public Alertdata: any;
  option = {
    params: new HttpParams().append('token', localStorage.getItem('token')),
  };
  findFactoryByuser = 'api/Factories/factory/ByUser';
  options = [];
  test = this.http.post(this.findFactoryByuser, {},
    this.option).subscribe(data => {
    const resSTR = JSON.stringify(data);
    const resJSON = JSON.parse(resSTR);
    this.options = resJSON;
  });


  state: boolean;

  ok(event) {
    this.http.post('api/sensors/sensor/findByCode', {code: event},
      this.option).subscribe(data => {
      const resSTR = JSON.stringify(data);
      const resJSON = JSON.parse(resSTR);
      this.data2 = resJSON;
      this.http.post('api/sensors/sensor/actuator/', {
          state: !this.data2[0].state,
          code: event,
        },
        this.option).subscribe(data1 => {
      });
      if (this.data2[0].state) {
        Swal.fire('OFF!',
          'Your light device is OFF now.',
          'error',
        );
      } else Swal.fire('ON!',
        'Your light device is ON now.',
        'success',
      );
    });


  }

  filter() {
    this.service.Data(this.selectedOption._id);
  }

  filter2() {
    this.service.Data({value: 'nan'});
  }

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
    dark: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.lightCard,
        type: 'warning',
      },
      /* {
         ...this.rollerShadesCard,
         type: 'primary',
       },
       {
         ...this.wirelessAudioCard,
         type: 'danger',
       },
       {
         ...this.coffeeMakerCard,
         type: 'info',
       },*/
    ],
    dark: this.commonStatusCardsSet,
  };

  constructor(private themeService: NbThemeService,
              private solarService: SolarData,
              private searchService: NbSearchService,
              private http: HttpClient,
              private  service: DashService,
              private toastrService: NbToastrService) {
    this.service.currentRelayMessage.subscribe(data => {

      if (data.value === 'nan') {
        this.getData();
      } else {
        this.id_fac = data;
        const option = {
          params: new HttpParams().append('token', localStorage.getItem('token')),
        };
        this.http.post('api/sensors/sensor/findByType',
          {
            type: 'lightActuator',
            factoryId: this.id_fac,
          }, option).subscribe(data2 => {
          const resSTR = JSON.stringify(data2);
          const resJSON = JSON.parse(resSTR);
          this.data = resJSON;
        });
      }
    });
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
      });

    this.solarService.getSolarData()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.solarValue = data;
      });
    this.getSensor();
  }

  getSensor() {
    const option = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/sensors/sensor/findByType',
      {
        type: 'Sensor',
        factoryId: this.id_fac,
      }, option).subscribe(data2 => {
      const resSTR = JSON.stringify(data2);
      const resJSON = JSON.parse(resSTR);
      this.data3 = resJSON;
      this.data3.forEach(item => {
        this.NotifToast(item);
      });
    });
  }

  NotifToast(item) {
    const option = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/alerts/alert/ToastNotification',
      {
        code: item._id,
      }, option).subscribe(data2 => {
      const resSTR = JSON.stringify(data2);
      const resJSON = JSON.parse(resSTR);
      this.Alertdata = resJSON;
      const i = item.data.length - 1;
      this.Alertdata.forEach(alert => {
        //  console.log(+ item.data[i].tempValues + '<' + alert.min + '||' + item.data[i].tempValues + '>' + alert.max);
        if (alert.data === 'Temperature') {
          if (item.data[i].tempValues < alert.min || item.data[i].tempValues > alert.max) {
            this.content = 'temperature of device  ' + item.name + ' is : ' + item.data[i].tempValues + ' °c';
            this.status = alert.status;
            this.title = alert.status;
            const HV = item.data[i].tempValues;
            const HN = item.name;
            this.duration = 10000;
            const dataa = 'Humidity';
            if (alert.Nemail === 'true') {
              this.sendEmail(item, alert, HV, HN, dataa, this.status);
            }
            this.makeToast();
          }
        }
        if (alert.data === 'Humidity') {
          if (item.data[i].humValues < alert.min || item.data[i].humValues > alert.max) {
            this.content = 'Humidity of device  ' + item.name + ' is : ' + item.data[i].humValues + ' %';
            const HV = item.data[i].humValues;
            const HN = item.name;
            this.status = alert.status;
            this.duration = 10000;
            this.title = alert.status;
            const dataa = 'Humidity';
            this.makeToast();
            if (alert.Nemail === 'true') {
              this.sendEmail(item, alert, HV, HN, dataa, this.status);
            }
          }
        }
      });
    });

  }

  sendEmail(item, alert, value, name, data, status) {
    const option = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/alerts/alert/SendEmail',
      {
        device: item.code,
        Nemail: alert.Nemail,
        value: value,
        name: name,
        data: data,
        status: status,
      }, option).subscribe(data2 => {
      const resSTR = JSON.stringify(data2);
      const resJSON = JSON.parse(resSTR);
      this.data3 = resJSON;
    });
  }

  makeToast() {
    this.showToast(this.status, this.title, this.content);
  }

  config: ToasterConfig;
  index = 1;
  destroyByClick = true;
  duration = 6000;
  hasIcon = true;
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = 'danger';
  title = ' Alert !!';
  content = '';

  private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: this.destroyByClick,
      duration: this.duration,
      hasIcon: true,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title;
    this.index += 1;
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  getData() {
    const option = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/sensors/sensor/findByType',
      {
        type: 'lightActuator',
      }, option).subscribe(data => {
      const resSTR = JSON.stringify(data);
      const resJSON = JSON.parse(resSTR);
      this.data = resJSON;

    });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
