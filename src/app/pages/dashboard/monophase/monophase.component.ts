
import {AfterViewInit, Component, Input, OnDestroy} from '@angular/core';
import {NbThemeService} from '@nebular/theme';
import {HttpClient, HttpParams} from '@angular/common/http';
import * as XLSX from 'xlsx';

declare const echarts: any;
const Swal = require('sweetalert2');
@Component({
  selector: 'ngx-monophase',
  styleUrls: ['./monophase.component.scss'],
  templateUrl: './monophase.component.html',
})
export class MonophaseComponent {
  fileName = 'ExcelSheett.xlsx';
  private data;
  private value = 0;
  private GazValue;
  ComActiveTotalEnergy;
  PositiveActiveTotalEnergy;
  ReverseActiveTotalEnergy;
  Voltage;
  Current;
  ActivePower;
  PowerGridFrequency;
  PowerFactor;
  last_updateEXC ;
  // chars consomation active energy
   canvasWidth = 200 ;
   needleValue_com_total_energy  ;
   label_com_total_energy = '' ;

   needleValue_PositiveActiveTotalEnergy  ;
   label_PositiveActiveTotalEnergy = '' ;
  //
   needleValue_ReverseActiveTotalEnergy  ;
   label_ReverseActiveTotalEnergy = '' ;
  //
   needleValue_ActivePower  ;
   label_ActivePower = '' ;
  last_update ;

  public bottomLabel = 'Kwh'  ;
  public options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '100'],
    needleStartValue: 20,
  } ;
  public options2 = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '120'],
    needleStartValue: 20,
  } ;
  public options3 = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '240'],
    needleStartValue: 20,
  } ;
  public options4 = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '50'],
    needleStartValue: 30,
  } ;
  themeSubscription: any;
  name1: any = 'Com Active Total Energy';
  name2: any = 'Positive Active Total Energy';
  name3: any = 'Reverse Active Total Energy';
  name4: any = 'ActivePower';

  constructor(private theme: NbThemeService, private http: HttpClient) {
    const optionss = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/sensors/sensor/findByType',
      {
        type: 'mono',
      }, optionss).subscribe(data => {
      const resSTR = JSON.stringify(data);
      const resJSON = JSON.parse(resSTR);
      this.data = resJSON;
      this.data.forEach(item => {
        const c = item.Countersdata.length - 1;
        this.PositiveActiveTotalEnergy = item.Countersdata[c].PositiveActiveTotalEnergy;
        this.ReverseActiveTotalEnergy = item.Countersdata[c].ReverseActiveTotalEnergy;
        this.Voltage = item.Countersdata[c].Voltage;
        this.Current = item.Countersdata[c].Current;
        this.ActivePower = item.Countersdata[c].ActivePower;
        this.PowerGridFrequency = item.Countersdata[c].PowerGridFrequency;
        this.PowerFactor = item.Countersdata[c].PowerFactor;

        this.label_com_total_energy = item.Countersdata[c].ComActiveTotalEnergy.substring(4 , 10) ;
        this.needleValue_com_total_energy = 2;
        // this.option.arcDelimiters = item.Countersdata[c].ComActiveTotalEnergy ;
        //
        this.label_PositiveActiveTotalEnergy = item.Countersdata[c].PositiveActiveTotalEnergy.substring(4 , 10) ;
        this.needleValue_PositiveActiveTotalEnergy = item.Countersdata[c].PositiveActiveTotalEnergy ;
        //  this.option.arcDelimiters = item.Countersdata[c].PositiveActiveTotalEnergy ;
        //
        this.label_ReverseActiveTotalEnergy = item.Countersdata[c].ReverseActiveTotalEnergy.substring(4 , 10) ;
        this.needleValue_ReverseActiveTotalEnergy = item.Countersdata[c].ReverseActiveTotalEnergy ;
        //   this.option.arcDelimiters = item.Countersdata[c].ReverseActiveTotalEnergy ;
        //
        this.label_ActivePower = item.Countersdata[c].ActivePower.substring(4 , 10) ;
        this.needleValue_ActivePower = item.Countersdata[c].ActivePower ;
        // this.option.arcDelimiters = item.Countersdata[c].ActivePower ;
        this.last_update = new Date (item.Countersdata[c].time ) ;
        const date = new Date (item.Countersdata[c].time ) ;
               let min;
 date.getMinutes() > 10 ? (min = date.getMinutes()) : min = '0' + date.getMinutes();
 this.last_updateEXC = (date.getFullYear() + '/' + date.getDate() +
   '/' + (date.getMonth() + 1) + ' ' + date.getHours() + ':' + min + ' ');
      });
    });
  }
  onChange(event) {
    if (event === true) {
      Swal.fire('ON!',
        'Your Electric meter device is ON Now.',
        'success',
      );
    } else {
      Swal.fire('OFF!',
        'Your  Electric meter device is OFF Now.',
        'error',
      );
    }
  }

  exportexcel(): void {
    /* table id is passed over here */
    const element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);

  }
}
