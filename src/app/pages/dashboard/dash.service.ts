import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root',
})
export class  DashService {
  ChartUpdate = io.connect('http://localhost:3000/Sensor/UpdateValue');
  private ChartUpdateSource = new BehaviorSubject<any>({value : 'nan'});
  ChartUpdateValue = this.ChartUpdateSource.asObservable();

  updateChartRequired = true;
  constructor() {
    this.ChartUpdate.on('setChartdata', (dataa) => {
       console.log('setchardata' , dataa);
      this.ChartUpdateSource.next(dataa);
    });
  }
  private filter = new BehaviorSubject<any>({value: 'nan'});
  currentRelayMessage = this.filter.asObservable();

  Data(id) {
    this.filter.next(id);
  }

  private stat = new BehaviorSubject<any>({value: 'nan'});
  dataMsg = this.stat.asObservable();

  DataStat(item) {
    this.stat.next(item);
  }

  ChartUpdatefunction(token , locationId) {
    // console.log('updated Chart');
    if (this.updateChartRequired) {
      this.ChartUpdate.emit('getChartdata', {Accesstoken: token, LocationId: locationId});
      this.updateChartRequired = false;
      // this.ChartUpdate.disconnect();
      // console.log('updated Chart');
    }
  }

/*********** soket it */

  Update = io.connect('http://localhost:3000/hello');

}
