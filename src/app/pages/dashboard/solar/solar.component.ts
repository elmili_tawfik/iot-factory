import { AfterViewInit, Component, Input, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import {HttpClient, HttpParams} from '@angular/common/http';

const Swal = require('sweetalert2');
@Component({
  selector: 'ngx-solar',
  styleUrls: ['./solar.component.scss'],
  templateUrl: './solar.component.html',
})
export class SolarComponent {
  last_update ;
  private  data ;
  private value = 0;
  private GazValue ;
 /* public canvasWidth = 500 ;
  private needleValue  ;
  public centralLabel = '' ;
  public bottomLabel = ''  ;
  public options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [50],
    rangeLabel: ['0', '3'],
    needleStartValue: 20,
  } ;*/
  public canvasWidth = 450 ;
  public needleValue  ;
  public centralLabel = '' ;
  public bottomLabel  ;
  public options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [30],
    rangeLabel: ['0', '100'],
  };
val ;
  @Input('chartValue')
  set chartValue(value: number) {
    this.value = 10;

    if (this.option.series) {
      this.option.series[0].data[0].value = 50;
      this.option.series[0].data[1].value = 100 - value;
      this.option.series[1].data[0].value = value;
    }
  }

  option: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService , private http: HttpClient) {
    const optionss = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/sensors/sensor/findByType',
      {
        type : 'GasMeter',
      }, optionss).subscribe(data => {
      const resSTR = JSON.stringify(data);
      const resJSON = JSON.parse(resSTR);
      this.data = resJSON;
      this.data.forEach(item => {
        const c = item.Countersdata.length - 1 ;
        this.bottomLabel = item.Countersdata[c].Gas_data.substring(2 , 8) ;
        this.val = item.Countersdata[c].Gas_data.substring(2 , 8) ;
        this.last_update = new Date(item.Countersdata[c].time) ;
        this.needleValue = 2 ;
      });
    });
  }

  onChange(event) {
    if (event === true) {
      Swal.fire('ON!',
        'Your Gas meter device is ON Now.',
        'success',
      );
    } else {
      Swal.fire('OFF!',
        'Your  Gas meter device is OFF Now.',
        'error',
      );
    }
  }

}
