import { delay } from 'rxjs/operators';
import { AfterViewInit, Component, Input, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import {HttpClient, HttpParams} from '@angular/common/http';
import any = jasmine.any;
const Swal = require('sweetalert2');
declare const echarts: any;

@Component({
  selector: 'ngx-water',
  styleUrls: ['./waterMeter.component.scss'],
  templateUrl: './waterCounter.component.html',
})
export class WaterCounterComponent implements AfterViewInit, OnDestroy {
  last_update ;
  private  data ;
  private value = 0;
  private waterValue ;
  public canvasWidth = 450 ;
  public needleValue  ;
  public centralLabel = '' ;
  public bottomLabel   ;
  public options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 10,
    arcColors: ['rgb(44, 151, 222)', 'lightgray'],
    arcDelimiters: [30],
    rangeLabel: ['0', '100'],
    needleStartValue: 20,
  } ;
  @Input('chartValue')
  set chartValue(value: number) {
    this.value = 20;

    if (this.option.series) {
      this.option.series[0].data[0].value = value;
      this.option.series[0].data[1].value = 100 - value;
      this.option.series[1].data[0].value = value;
    }
  }

  option: any = {};
  themeSubscription: any;

  constructor(private theme: NbThemeService , private http: HttpClient) {
    const optionss = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/sensors/sensor/findByType',
      {
        type : 'WaterMeter',
      }, optionss).subscribe(data => {
      const resSTR = JSON.stringify(data);
      const resJSON = JSON.parse(resSTR);
      this.data = resJSON;
      this.data.forEach(item => {
        const i = item.Countersdata.length - 1 ;
       // this.centralLabel = item.Countersdata[i].Water_data.substring(4 , 8) ;
        this.needleValue = item.Countersdata[i].Water_data ;
        this.bottomLabel = item.Countersdata[i].Water_data ;
        console.log('date mta3 water ', item.Countersdata[i].time) ;
        this.last_update = new Date(item.Countersdata[i].time);
       /*       let min;
        date.getMinutes() > 10 ? (min = date.getMinutes()) : min = '0' + date.getMinutes();
        this.last_update =(date.getFullYear() + '/' + date.getDate() +
          '/' + (date.getMonth() + 1) + ' ' + date.getHours() + ':' + min + ' ');*/

      });
    });
  }

  ngAfterViewInit() {
    this.themeSubscription = this.theme.getJsTheme().pipe(delay(1)).subscribe(config => {

      const solarTheme: any = config.variables.solar;

      this.option = Object.assign({}, {
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c} ({d}%)',
        },
        series: [
          {
            name: ' ',
            clockWise: true,
            hoverAnimation: false,
            type: 'pie',
            center: ['45%', '50%'],
            radius: solarTheme.radius,
            data: [
              {
                value: 20,
                name: ' ',
                label: {
                  normal: {
                    position: 'center',
                    formatter: '{d}%',
                    textStyle: {
                      fontSize: '22',
                      fontFamily: config.variables.fontSecondary,
                      fontWeight: '600',
                      color: config.variables.fgHeading,
                    },
                  },
                },
                tooltip: {
                  show: false,
                },
                itemStyle: {
                  normal: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                      {
                        offset: 0,
                        color: solarTheme.gradientLeft,
                      },
                      {
                        offset: 1,
                        color: solarTheme.gradientRight,
                      },
                    ]),
                    shadowColor: solarTheme.shadowColor,
                    shadowBlur: 0,
                    shadowOffsetX: 0,
                    shadowOffsetY: 3,
                  },
                },
                hoverAnimation: false,
              },
              {
                value: 100 ,
                name: ' ',
                tooltip: {
                  show: false,
                },
                label: {
                  normal: {
                    position: 'inner',
                  },
                },
                itemStyle: {
                  normal: {
                    color: solarTheme.secondSeriesFill,
                  },
                },
              },
            ],
          },
          {
            name: ' ',
            clockWise: true,
            hoverAnimation: false,
            type: 'pie',
            center: ['45%', '50%'],
            radius: solarTheme.radius,
            data: [
              {
                value: this.value,
                name: ' ',
                label: {
                  normal: {
                    position: 'inner',
                    show: false,
                  },
                },
                tooltip: {
                  show: false,
                },
                itemStyle: {
                  normal: {
                    color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                      {
                        offset: 0,
                        color: solarTheme.gradientLeft,
                      },
                      {
                        offset: 1,
                        color: solarTheme.gradientRight,
                      },
                    ]),
                    shadowColor: solarTheme.shadowColor,
                    shadowBlur: 7,
                  },
                },
                hoverAnimation: false,
              },
              {
                value: 28,
                name: ' ',
                tooltip: {
                  show: false,
                },
                label: {
                  normal: {
                    position: 'inner',
                  },
                },
                itemStyle: {
                  normal: {
                    color: 'none',
                  },
                },
              },
            ],
          },
        ],
      });
    });
  }
  getstatus (value) {

    if (value <= 25) {
      return 'danger';
    } else if (value <= 50) {
      return 'warning';
    } else if (value <= 75) {
      return 'info';
    } else {
      return 'success';
    }
  }
 /*getData() {
  const optionss = {
    params: new HttpParams().append('token', localStorage.getItem('token')),
  };
  this.http.post('api/sensors/sensor/findByType',
    {
      type : 'Esonsor',
    }, optionss).subscribe(data => {
    const resSTR = JSON.stringify(data);
    const resJSON = JSON.parse(resSTR);
    this.data = resJSON;
    this.data.forEach(item => {
      const c = item.otherVal.length - 1 ;
      this.Evalue = item.otherVal[c] ;

    });
  });
}*/
  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }

  onChange(event) {
    if (event === true) {
      Swal.fire('ON!',
        'Your Water meter device is ON Now.',
        'success',
      );
    } else {
      Swal.fire('OFF!',
        'Your  Water meter device is OFF Now.',
        'error',
      );
    }
  }

}
