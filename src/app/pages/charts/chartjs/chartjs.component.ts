import {Component} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {DashService} from '../../dashboard/dash.service';
import any = jasmine.any;

@Component({
  selector: 'ngx-chartjs',
  styleUrls: ['./chartjs.component.scss'],
  templateUrl: './chartjs.component.html',
})
export class ChartjsComponent {

  private Sdata = [];
  private token;
  selectedOption: any = 'ok';

  constructor(private http: HttpClient, private  service: DashService) {
    this.token = {
      params: new HttpParams().append('token', localStorage.getItem('token')),
    };
    this.http.post('api/sensors/sensor/findByType',
      {
        type: 'Sensor',
      }, this.token).subscribe(data => {
      const resSTR = JSON.stringify(data);
      const resJSON = JSON.parse(resSTR);
      this.Sdata = resJSON;
    });

  }

  send() {
    this.service.DataStat(this.selectedOption);

  }
}
