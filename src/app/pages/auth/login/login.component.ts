import { Component , OnInit } from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import {HttpClient} from '@angular/common/http';
import { FormBuilder, FormGroup, Validators , FormControl } from '@angular/forms';

import {Router} from '@angular/router';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent implements OnInit {
  constructor(private http: HttpClient, private router: Router, private formBuilder: FormBuilder) {
    localStorage.clear();
  }

  loginForm = new FormGroup({
    email: new FormControl(),
    password: new FormControl(),
  });
  submitted = false;

  UserapiUrl = 'api/users/login';
msg: string ;
  onSubmit() {

    this.submitted = true ;
    if (this.loginForm.valid) {
    this.http.post(this.UserapiUrl,
      {
        email: this.loginForm.get('email').value,
        password: this.loginForm.get('password').value,
      }).subscribe(data => {
      const resSTR = JSON.stringify(data);
      const resJSON = JSON.parse(resSTR);
       if (resJSON.status === 'err') {
         this.msg = 'wrong password or email';
      } else {
         this.msg = '' ;
         this.router.navigate(['/pages/iot-dashboard']);
         localStorage.setItem('currentUser', JSON.stringify(resJSON.UserData[0]));
         localStorage.setItem('token', resJSON.token.toString());
      }
    }, error => {
    });
  }}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],

    });
  }

  get f() {
    return this.loginForm.controls;
  }
}
