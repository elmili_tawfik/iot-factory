/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component , OnInit } from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';
import {HttpClient} from '@angular/common/http';
import { FormBuilder, FormGroup, Validators , FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  templateUrl: './reset-password.component.html',
})
export  class NgxResetPasswordComponent {

}
