import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'ngx-req-code',
  templateUrl: './req-code.component.html',
  styleUrls: ['./req-code.component.scss'] ,
})
export class NgxReqCodeComponent implements OnInit {
  constructor( private http: HttpClient, private router: Router, private formBuilder: FormBuilder) {
  }

  reqCodetForm = new FormGroup({
    code: new FormControl(),
  });
  submitted = false;
  msg: string ;
  onSubmit() {
    this.submitted = true ;
    if (this.reqCodetForm.valid) {
      this.http.post('api/users/code',
        {
          email: this.reqCodetForm.get('code').value,
        }).subscribe(data => {
        const resSTR = JSON.stringify(data);
        const resJSON = JSON.parse(resSTR);
        if (resJSON.status === 'err') {
          this.msg = 'wrong code';
          this.router.navigate(['/auth/request']);
        } else {
          this.msg = '' ;
          this.router.navigate(['/auth/reset']);
        }
      }, error => {
      });
    }}
  ngOnInit() {
  }
}
