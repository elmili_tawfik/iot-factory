export  class User {
  id: string;
  username: string;
  email: string;
  password: string;
  numTel: number;
  terms?: boolean;
  confirmpassword: string;
}
