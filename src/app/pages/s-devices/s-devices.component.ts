import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {of} from 'rxjs';

const Swal = require('sweetalert2');

@Component({
  selector: 'ngx-s-devices',
  templateUrl: './s-devices.component.html',
  styleUrls: ['./s-devices.component.scss'],
})
export class SDevicesComponent {


  constructor(private http: HttpClient) {
  }

  selectedRows;
  SensorUrl = 'api/sensors/sensor/findByUser';
  settings = {
    filtering : false ,

    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',

      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"  style="\n' +
        '    color: red;"></i>',
      confirmDelete: true,
    },
    columns: {
      code: {
        title: 'ID',
        type: 'string',
      },
      name: {
        title: 'Sensor name',
        type: 'string',
      },
      type: {
        title: 'Type',
        type: 'string',
      },
      factoryName: {
        title: 'Factory Name  ',
        type: 'string',
      },
      area: {
        title: ' area ',
        type: 'string',
      },
    },
  };


  onEditConfirm(event): void {
    this.http.post('/api/sensors/sensor/update/',
      {
        id: event.newData._id,
        name: event.newData.name ,
        area : event.newData.area ,
      }, this.options).subscribe(
      res => {
        event.confirm.resolve(event.newData);
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Your device has been updated',
          showConfirmButton: false,
          timer: 1500 ,
        }) ;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
        } else {
        }
      });
  }

  onDeleteConfirm(event): void {

    Swal.fire({
      title: 'Are you sure?',
      text: 'You won"t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        this.http.post('/api/sensors/sensor/updateUserAndFactory/' + event.data.code, {}, this.options).subscribe();
        event.confirm.resolve();
        Swal.fire(
          'Success!',
          'Your Device has been deleted .',
          'success',
        );
      }
    });

  }

  options = {
    params: new HttpParams().append('token', localStorage.getItem('token')),
  };
  data = this.http.post(this.SensorUrl,
    {}, this.options).subscribe(data => {
    const resSTR = JSON.stringify(data);
    const resJSON = JSON.parse(resSTR);
    this.data = resJSON;
  });

  onCreateConfirm($event: any) {
  }

  // text

  private setting = {
    element: {
      dynamicDownload: null as HTMLElement ,
    },
  } ;
  dynamicDownloadTxt() {
      this.dyanmicDownloadByHtmlTag({
        fileName: 'Annex1',
        text: '03030303030303030303030303030565656065\n',
      });
  }  dynamicDownloadTxt1() {
      this.dyanmicDownloadByHtmlTag({
        fileName: 'Annex2',
        text: '03030303030303030303030303030565656065\n',
      });
  }
  private dyanmicDownloadByHtmlTag(arg: {
    fileName: string,
    text: string ,
  }) {
    if (!this.setting.element.dynamicDownload) {
      this.setting.element.dynamicDownload = document.createElement('a');
    }
    const element = this.setting.element.dynamicDownload;
    const fileType = arg.fileName.indexOf('.json') > -1 ? 'text/json' : 'text/plain';
    element.setAttribute('href', `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`);
    element.setAttribute('download', arg.fileName);

    const event = new MouseEvent('click');
    element.dispatchEvent(event);
  }
}
