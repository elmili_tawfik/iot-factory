import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Developed by <b>
      <a href="https://www.facebook.com/tawfikmilii" target="_blank">Elmili Tawfik - Treetronix Technology&copy; 2020 </a></b></span>
    <div class="socials">
      <a href="https://www.facebook.com/tawfikmilii" target="_blank" class="ion ion-social-facebook"></a>
      <a href="#" target="_blank" class="ion ion-social-twitter"></a>
      <a href="https://www.linkedin.com/in/tawfik-el-mili-1baa83130/" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
